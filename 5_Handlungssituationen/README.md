# Handlungsziele und Handlungssituationen
Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden. Die Handlungsziele sind auf die Module PRJ1 bis PRJ5 verteilt und bauen systematisch aufeinander auf.

---

## PRJ1: Kommunikation und Teamdynamik
### 1. Kommunikation situationsangepasst und wirkungsvoll gestalten  
Das Entwicklungsteam stellt fest, dass die Einbindung einer externen Schnittstelle mehr Aufwand macht als geplant. Als Projektleiter kommunizieren Sie diese Verzögerung zeitnah und erklären die Gründe sachlich und klar. Sie passen die Kommunikation an die Bedürfnisse der Stakeholder an und präsentieren Lösungsvorschläge.

### 2. Teamführung und Sozialkompetenz entwickeln  
Ihr Projektteam hat Schwierigkeiten in der Zusammenarbeit. Als Projektleiter moderieren Sie ein Meeting, um Konflikte zu besprechen, die Aufgaben neu zu verteilen und sicherzustellen, dass alle Teammitglieder motiviert und zielgerichtet arbeiten.

---

## PRJ2: Projektsteuerung und Risikoanalyse
### 3. ICT-Projekte eigenständig bis zur Ausführungsreife planen und steuern  
Sie planen die Einführung einer neuen Software zur Optimierung von Geschäftsprozessen. Dabei erstellen Sie Projektpläne, definieren Ressourcen und überwachen kontinuierlich den Fortschritt, um sicherzustellen, dass das Projekt im Rahmen der Vorgaben abgeschlossen wird.

### 4. Technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen  
Bei der Installation eines neuen Druckers führen Sie eine technische Risikoanalyse durch. Sie identifizieren potenzielle Risiken, z. B. Lieferverzögerungen oder Integrationsprobleme, und entwickeln entsprechende Massnahmen zur Risikominimierung.

---

## PRJ3: Leitung einer ICT-Organisationseinheit
### 5. Ressourcen für eine ICT-Organisationseinheit planen und budgetieren  
Sie planen die Ressourcen und das Budget für die Entwicklung eines neuen Druckertreibers. Dabei stellen Sie sicher, dass die erforderlichen Ressourcen termingerecht verfügbar sind und die Kommunikation mit Stakeholdern reibungslos funktioniert.

### 6. Den Informationsbedarf für Entscheidungssituationen bestimmen  
Sie leiten die Auswahl einer neuen ERP-Software-Lösung. Um eine fundierte Entscheidung zu treffen, analysieren Sie den Informationsbedarf aller Anspruchsgruppen (IT-Team, Führungskräfte, Endbenutzer) und präsentieren geeignete Lösungsvorschläge.

---

## PRJ4: Strategisches Management und Kommunikation
### 7. Strategische Entscheidungen zur Effizienzsteigerung treffen  
Ihr Unternehmen strebt die Optimierung der ICT-Prozesse an. Als Leiter der Organisationseinheit entwickeln Sie langfristige Strategien zur Effizienzsteigerung und präsentieren Ihre Vorschläge der Geschäftsleitung.

### 8. Präventives Risikomanagement etablieren  
Bei der Umsetzung eines grossen Projekts implementieren Sie präventive Massnahmen, um potenzielle Risiken frühzeitig zu erkennen und abzufedern. Sie simulieren Krisenszenarien und entwickeln Notfallpläne.

---

## PRJ5: Abschluss und Optimierung
### 9. ICT-Projekte termin- und budgetgerecht abschliessen  
Sie leiten ein Projekt zur Entwicklung einer Software-Anwendung für einen Kunden. Sie stellen sicher, dass das Projekt im vorgesehenen Zeitrahmen und Budget abgeschlossen wird, und dokumentieren alle Ergebnisse für eine erfolgreiche Übergabe.

### 10. Konfliktmanagement und transparente Entscheidungsprozesse sicherstellen  
Während der Abschlussphase eines Projekts treten Konflikte zwischen Teammitgliedern und Stakeholdern auf. Sie implementieren gezielte Konfliktlösungsstrategien und moderieren Entscheidungsprozesse, um die Projektziele erfolgreich zu erreichen.

---
