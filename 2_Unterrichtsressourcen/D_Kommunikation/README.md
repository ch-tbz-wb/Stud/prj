# Inhalt der Kompetenz Projektkommunikation

[[_TOC_]]

In der Welt der Cloudservice-Engineering-Projekte spielt effektive Projektkommunikation eine entscheidende Rolle für den Erfolg. Angesichts der Komplexität moderner Cloud-Architekturen und der Vielfalt der Stakeholder, von Entwicklerteams über Management bis hin zu Endkunden, ist es unerlässlich, Informationen präzise, zielgerichtet und effektiv zu vermitteln. Die Fähigkeit, die Quantität und Qualität der Informationen adressatengerecht zu selektieren (A2.3) und Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat zu präsentieren (A2.4), ist von unschätzbarem Wert.

Cloudservice-Engineers stehen vor der Herausforderung, technische Details und Projektfortschritte so zu kommunizieren, dass sie von allen Stakeholdern verstanden und geschätzt werden. Dies erfordert nicht nur technisches Verständnis, sondern auch die Fähigkeit, komplexe Sachverhalte klar und überzeugend darzustellen. Initiative und Kreativität (B5.5) in der Kommunikation können dazu beitragen, innovative Lösungen für Projektprobleme zu finden und die Durchführung effizient zu gestalten.

Die Entwicklung von Ausweichplänen für potenzielle Umsetzungsprobleme (B5.7) und die sorgfältige Dokumentation zur Überwachung des Projektfortschritts (B5.9) sind weitere Schlüsselaspekte der Projektkommunikation. Diese Massnahmen helfen, Risiken zu minimieren und den Projektverlauf transparent und nachvollziehbar zu gestalten.

Um Projekte termin- und budgetgerecht sowie in Übereinstimmung mit den ursprünglichen Anforderungen abzuschliessen (B5.10), ist es unerlässlich, eine klare und kontinuierliche Kommunikation über den Projektstatus, mögliche Herausforderungen und Erfolge zu pflegen. Ebenso wichtig ist es, die Ressourcen für eine ICT-Organisationseinheit effektiv zu planen und zu budgetieren und den Mitarbeitereinsatz zu organisieren (B6.2). Dazu gehört auch, den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen zu bestimmen (B6.3), um sicherzustellen, dass alle Beteiligten die Informationen erhalten, die sie benötigen, um fundierte Entscheidungen zu treffen.


### Kompetenzen

* A2.3  Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen  
* A2.4  Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen  
* B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln  
* B5.9  Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen  
* B5.10  ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen  
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen  
* B6.3  Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen  

### Lernziele / Taxonomie 

**Quantität und Qualität der Informationen adressatengerecht selektieren (A2.3):**

***Lernziel***: Lernen, wie man relevante Informationen basierend auf dem Wissensstand und den Bedürfnissen der Stakeholder filtert und zusammenfasst.

***Praxisanwendung:*** Bei der wöchentlichen Projektbesprechung eines Cloud-Migrationsprojekts unterschiedliche Informationspakete für technische Teams und das Management vorbereiten, um sicherzustellen, dass beide Gruppen die für sie relevanten Informationen in angemessener Tiefe erhalten.

**Arbeitsergebnisse zielgruppenadäquat präsentieren (A2.4):**

***Lernziel:*** Fähigkeiten entwickeln, um Projektupdates und Ergebnisse mit passenden visuellen und rhetorischen Mitteln effektiv zu kommunizieren.

***Praxisanwendung:*** Eine interaktive Präsentation für das End-of-Sprint-Meeting erstellen, die den Fortschritt anhand von Diagrammen und Dashboards zeigt, um die erzielten Ergebnisse anschaulich und verständlich zu machen.

**Initiative und Kreativität in der Projektdurchführung (B5.5):**

***Lernziel:*** Kreatives Denken und proaktives Handeln bei der Lösung von Projektherausforderungen üben.

***Praxisanwendung:*** Entwicklung eines innovativen Ansatzes zur Optimierung der Cloud-Ressourcennutzung in einem Entwicklungsprojekt, um Kosten zu senken und die Effizienz zu steigern.

**Entwicklung von Ausweichplänen (B5.7):**

***Lernziel:*** Fähigkeit erwerben, vorausschauend potenzielle Probleme zu identifizieren und praktikable Ausweichpläne zu entwickeln.

***Praxisanwendung:*** Ein Backup-Plan für die Datenmigration in der Cloud, der bei Ausfall der primären Migrationstools aktiviert wird, um Zeitverluste zu minimieren.

**Überwachung des Projektfortschritts (B5.9):**

***Lernziel:*** Lernen, effektive Dokumentationssysteme zu erstellen und zu pflegen, die eine klare Übersicht über den Projektstatus bieten.

***Praxisanwendung:*** Einrichtung eines Dashboard-Tools zur Echtzeit-Überwachung des Fortschritts verschiedener Teams bei der Entwicklung einer Cloud-basierten Anwendung, einschliesslich Zeitrahmen und Budgetüberwachung.

**Projekte term- und budgetgerecht abschliessen (B5.10):**

***Lernziel:*** Kompetenz im Zeit- und Ressourcenmanagement entwickeln, um Projekte gemäss den initialen Planungen abzuschliessen.

***Praxisanwendung:*** Anwendung von Projektmanagement-Tools und -Techniken, um ein Sicherheitsaudit für Cloud-Dienste innerhalb des vorgegebenen Zeitraums und Budgets erfolgreich durchzuführen.

**Planung und Budgetierung von Ressourcen (B6.2):**

***Lernziel:*** Fähigkeiten in der strategischen Planung und Allokation von Ressourcen für IT-Projekte erwerben.

***Praxisanwendung:*** Entwicklung eines detaillierten Ressourcenplans für die Implementierung eines neuen Cloud-Service, inklusive Personaleinsatz und Budgetierung.

**Informationsbedarf für Entscheidungen bestimmen (B6.3):**

***Lernziel:*** Die Fähigkeit erlangen, den spezifischen Informationsbedarf verschiedener Stakeholder für fundierte Entscheidungen zu identifizieren.

***Praxisanwendung:*** Einrichtung eines regelmässigen Reporting-Systems, das dem Managementteam ermöglicht, auf Basis aktueller Leistungsdaten und Trendanalysen strategische Entscheidungen zu treffen.

### Transfer
Im Rahmen von der Semesterarbeit.

### Hands-on

<mark>Work in progress</mark>

### Links

m<mark>Work in progress</mark>

## Links

<mark>Work in progress</mark>

