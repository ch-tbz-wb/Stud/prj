## Tag 1:
- Zertifizierung Professional Scrum Master 1 vorstellen
- Fragen, welche Begriffe die Studierenden bereits kennen zu Agile, Scrum, Kanban und diese sammeln - offener Austausch
- Einführung zu der Agile Philosophie mit dem Agile Manifesto (Vergleich zu Wasserfall ziehen)
- Einführung zu Scrum
- Video zeigen (Scrum in 7 minutes)
- Theorie vorstellen zu:
    - Scrum Rollen
    - Scrum Events
- Scrum Ressourcen zeigen
- Scrum Open Assessment zeigen und durchspielen lassen

## Tag 2: 
- Repetition vom letzten Mal (Vorwissen aktivieren, offener Austausch)
- Quiz zur Vorbereitung auf die PSM1 Zertifizierung (Studierende mit dem Scrum Guide arbeiten lassen, um aktiv anhand von den Quiz fragen zu lernen)
- Theorie vorstellen zu Product Vision, Product Goal, User Stories mit Akzeptanzkriterien, Definition of Done, Sprint Planning
- Studierende planen ihren ersten Sprint anhang vom untenstehenden Auftrag

![Auftrag](sprint-1-tasks-part1.png)

## Tag 3
- Quiz zur Vorbereitung auf die PSM1 Zertifizierung (Studierende mit dem Scrum Guide arbeiten lassen, um aktiv anhand von den Quiz fragen zu lernen)
- Sprint Review vorbereiten
- Sprint Review durchführen
- Sprint Retrospektive durchführen
- Vorstellung [5 coole Arten eine Sprint Retrospektive durchzuführen](https://www.atlassian.com/blog/jira/5-fun-sprint-retrospective-ideas-templates)
- [Tool für Planning Poker](https://planningpokeronline.com/new-game/)

Mögliche Tools für Sprint Retro:
- Vorstellung: [Sprint Retro Tool mit Demo](https://secure.teamretro.com/demo/retrospectives/4ls-retrospective)
- [Sprint Retro Tool](https://kollabe.com/)