Aktuell ist das Verzeichnis "Scrum Master Zertifizierung" noch im Aufbau und "under Construction".

## Infos zur Zertifizierung
Läuft nicht ab
Kostet 200 USD
85% muss richtig sein, um zu bestehen. 

## Wichtige Links
Scrum Guide online: https://scrumguides.org/scrum-guide.html
Scrum Guide als PDF: https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf#zoom=100
Scrum Glossar: https://www.scrum.org/resources/scrum-glossary
Probeprüfung für die Zertifizierung: https://www.scrum.org/open-assessments/scrum-open






