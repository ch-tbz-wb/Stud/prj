## Erfahrungsaustausch
Tauschen Sie sich in Teams aus über das agile Arbeiten auf dem Arbeitsplatz. 
- Gibt es regelmässige Meetings?
- Wie kommt das im Team an? 
- Sehen Sie dadurch einen Mehrwert?

## Wichtige Theorie
Kurzübersicht zur wichtigsten Scrum hands-on Theorie: https://www.figma.com/board/I79KrKItmi31rzdTIMIyto/SCRUM-Pricing-Calculator

Kurzübersicht zu Scrum anhand eines Beispiels: https://www.figma.com/board/U1U3Rsksylm7znnS6O8Oi0/Pricing-Calculator-SCRUM

## Introduction to Scrum
Video anschauen: Introduction to Scrum - 7 Minutes
https://www.youtube.com/watch?v=9TycLR0TqFA&ab_channel=Uzility

## Lesen Sie den Scrum Guide durch:
Scrum Guide online: https://scrumguides.org/scrum-guide.html
Scrum Guide als PDF (EN empfohlen, da die Zertifizierung auf Englisch stattfindet): https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf#zoom=100
Scrum Guide alle Sprachen: https://scrumguides.org/download.html

## Scrum Glossary durchschauen
https://www.scrum.org/resources/scrum-glossary

## Problelauf Scrum Open Assessment durchspielen
https://www.scrum.org/open-assessments/scrum-open

## Online Quiz durchspielen
http://scrumquiz.org/#/scrum-master-practice-test 

## Scrum Quiz 1 und 2
Auf Socrative oder alternativ als PDF in diesem Verzeichnis.

