# Auftrag: Scrum in der Praxis – Realisiert euer eigenes Projekt

## Ziel
Ihr habt den Scrum Guide gelesen – jetzt ist es an der Zeit, Scrum in der Praxis anzuwenden. Euer Ziel ist es, als Team ein Produkt eurer Wahl zu entwickeln, das einen echten Mehrwert schafft. Ihr werdet mindestens 5 Sprints durchführen und dabei alle Scrum Events erleben, um euch als Team kontinuierlich zu verbessern.

## Vorgehensweise

### 1. Scrum Team gründen
- Findet euch als Team zusammen.
- Bestimmt eure Rollen: Product Owner, Scrum Master und Entwickler-Team.
- Achtet darauf, dass der Product Owner die Verantwortung für das Product Backlog übernimmt und der Scrum Master den Scrum-Prozess sicherstellt.

### 2. Projektidee entwickeln
- Überlegt euch, was ihr als Team realisieren möchtet.
- Diskutiert, welchen Mehrwert euer Produkt bringen soll.
- Dokumentiert eure **Produktvision**, um eine klare Richtung zu haben.

### 3. Product Goal festlegen
Scrum-Dokumentation durchlesen: [What is a Product Goal?](https://www.scrum.org/resources/what-product-goal?utm_source=google&utm_medium=adwords&utm_id=psmii&adgroup={groupid}&gad_source=1&gclid=Cj0KCQiA2oW-BhC2ARIsADSIAWq3SAspkdskhPPaOKA3mOif4SpdIFyeu8vheHRAsTDn4sSH25CPkNAaAl7TEALw_wcB)

Sehr gute Übersicht: [Product Goals: How to Write and Use Them](https://www.parabol.co/blog/product-goals/#:~:text=What%20is%20a%20Product%20Goal%20in%20Scrum%3F,team%27s%20short%2Dterm%20Sprint%20Goals.)

- Definiert, was das übergeordnete Ziel eures Produkts ist.
- Das Product Goal hilft euch, während des gesamten Projekts fokussiert zu bleiben.

### 4. Kommunikationswege festlegen
- Da ihr euch nur einmal pro Woche seht, überlegt euch, ob ein **Daily Scrum** virtuell stattfinden kann.
- Eine Möglichkeit ist, täglich in einem Chat alle Scrum-Fragen zu beantworten: 
  - Was habe ich gestern erreicht?
  - Was werde ich heute tun?
  - Welche Hindernisse gibt es?

### 5. Tools wählen
- Entscheidet gemeinsam, welche Tools ihr verwenden wollt, z. B.: 
  - **Trello / Jira** für das Backlog-Management
  - **Figma / Excalidraw** für visuelle Konzepte
  - **GitHub / GitLab** für die Softwareentwicklung
  - **Miro** für Kollaboration

### 6. Product Backlog erstellen
- Erstellt eine Liste mit **User Stories** und dazugehörigen Akzeptanzkriterien.
- Eine gute User Story folgt dem Format: "Als [Benutzer] möchte ich [Funktion], damit [Nutzen]."
- Priorisiert die User Stories nach Wichtigkeit.

Gute Übersicht zu [User Stories mit guten / schlechten Akzeptanzkriterien](https://www.smartsheet.com/content/user-story-with-acceptance-criteria-examples)

**Beispiel**

![User Story examples with acceptance criteria](./user-story-examples-with-acceptance-criteria.jpg)

### 7. Sprint Planning durchführen
- Wählt für den ersten Sprint einige der wichtigsten User Stories aus.
- Schätzt den Aufwand pro Story (z. B. mit Story Points oder T-Shirt-Sizes).
- Teilt die Aufgaben untereinander auf.

Konkreterer Auftrag zum ersten Sprint Planning

![Konkreterer Auftrag zum ersten Sprint Planning](sprint-1-tasks-part1.png)

[Video: The BEST Way to Run a Sprint Planning Meeting!](https://www.youtube.com/watch?v=2A9rkiIcnVI&ab_channel=upGradKnowledgeHut)
[Video: Tipps für ein erfolgreiches Sprint-Planungsmeeting](https://www.youtube.com/watch?v=9NWbQlRcdh0&ab_channel=Atlassian)
[Text: The Ultimate Agile Sprint Planning Guide](https://www.easyagile.com/blog/agile-sprint-planning)



### 8. Sprint durchführen
- Setzt die geplanten Aufgaben um.
- Falls ihr Hindernisse habt, bringt sie ins Daily Scrum ein.
- Nutzt euren Chat für tägliche Updates.

### 9. Sprint Review
- Am Ende des Sprints präsentiert ihr das fertige Inkrement.
- Holt Feedback ein: Erzeugt euer Produkt bereits Mehrwert?
- Falls nötig, passt das Product Backlog an.

[Video: Sprint Review Meetings](https://www.youtube.com/watch?v=InXAS_zRvqQ&list=PLaD4FvsFdarT0B2yi9byhKWYX1YmrkrpC&index=9&ab_channel=Atlassian)

### 10. Sprint Retrospektive
- Reflektiert, was gut lief und was verbessert werden kann.
- Definiert konkrete Verbesserungen für den nächsten Sprint.

[Video: Tipps für agile Retrospektiven](https://www.youtube.com/watch?v=dJNCHvqKljU&list=PLaD4FvsFdarT0B2yi9byhKWYX1YmrkrpC&index=10&ab_channel=Atlassian)

### 11. Sprint Burndown-Chart erstellen
- Dokumentiert eure Fortschritte visuell.
- Ein Sprint Burndown-Chart hilft, den Fortschritt über die Sprintdauer hinweg zu verfolgen.

## Erwartung
- Führt mindestens 5 Sprints durch.
- Durchlebt und dokumentiert alle Scrum Events.
- Haltet die Scrum Prinzipien ein (Transparenz, Inspektion, Adaption).
- Stellt am Ende ein funktionierendes Produkt vor, das echten Mehrwert bietet.

## Abgabe
- Dokumentiert eure Arbeit in einer kurzen Reflexion.
- Zeigt auf, wie ihr Scrum angewendet habt und was ihr gelernt habt.
- Präsentiert euer fertiges Produkt.

**Viel Erfolg – und denkt daran: Scrum lernt man am besten durch Tun!**

