## Projekt
Mehr Infos dazu folgen im Verlaufe des Unterrichts und werden hier ergänzt.

1 Woche Sprint empfohlen
Daily Scrum schriftlich machen

- 4 Teams erstellen 
- Jeder macht sich Gedanken zu einem möglichen Projekt
- Gegenseitig Projektideen austauschen und sich pro Gruppe für 1 Projekt entscheiden
- Product Backlog erstellen mit Features (in JIRA: Epics) und verlinkten Userstories (weitere Ressourcen dazu sind Teil von Requirements Engineering)
- Sprint Retrospektive und Sprint Review am Anfang jeder Woche

### Weitere Themen
- Scrum auf Papier möglich?
- Jira und andere Möglichkeiten Scrum zu machen
- Sprint Retro Tools