# Inhalt der Kompetenz Risikomanagement

[[_TOC_]]

Risikomanagement ist ein kritischer Aspekt in der Durchführung von Informatikprojekten, besonders für Cloudservice-Engineers, die sich mit der zunehmenden Komplexität und den sich schnell ändernden Anforderungen in Cloud-Umgebungen auseinandersetzen müssen. Die Fähigkeit, potenzielle Risiken frühzeitig zu identifizieren, zu analysieren und darauf zu reagieren, ist entscheidend, um Projekte erfolgreich und sicher durchzuführen.

Ein effektives Risikomanagement beginnt mit der systematischen Identifizierung von Risiken, die die Zielerreichung des Projekts beeinträchtigen könnten. Dies umfasst technische, organisatorische, finanzielle und rechtliche Risiken. Für Cloudservice-Engineers bedeutet dies insbesondere, auf spezifische Cloud-bezogene Risiken wie Datenschutzverletzungen, Ausfallzeiten der Dienste und Kompatibilitätsprobleme mit bestehenden Systemen zu achten.

Nach der Identifizierung der Risiken ist die Bewertung ihrer potenziellen Auswirkungen und Wahrscheinlichkeiten der nächste Schritt. Dies hilft, Prioritäten zu setzen und zu entscheiden, welche Risiken akzeptiert, vermieden, gemildert oder übertragen werden sollen. Beispielsweise könnte das Risiko eines Datenverlusts durch regelmässige Backups und die Implementierung robuster Sicherheitsprotokolle gemildert werden.

Die Entwicklung von Ausweichplänen ist ein weiterer zentraler Bestandteil des Risikomanagements. Diese Pläne legen fest, wie auf eingetretene Risiken reagiert werden soll, um die Auswirkungen zu minimieren und eine schnelle Rückkehr zum normalen Projektverlauf zu ermöglichen. Für Cloudservice-Engineers könnte ein Ausweichplan beispielsweise den Wechsel zu einem alternativen Cloud-Dienstanbieter oder die temporäre Nutzung von On-Premises-Ressourcen umfassen, falls der primäre Cloud-Dienst ausfällt.

Abschliessend ist die kontinuierliche Überwachung und Anpassung des Risikomanagementplans im Verlauf des Projekts unerlässlich. Dies erfordert eine regelmässige Überprüfung der Risikolandschaft, da sich neue Risiken ergeben oder sich die Einschätzungen bestehender Risiken ändern können. Eine offene Kommunikation im Projektteam und mit den Stakeholdern über Risiken und deren Management trägt wesentlich dazu bei, Überraschungen zu vermeiden und das Vertrauen in die Projektführung zu stärken.

Für Cloudservice-Engineers bietet ein proaktives Risikomanagement nicht nur die Möglichkeit, potenzielle Probleme zu vermeiden oder zu mildern, sondern es fördert auch eine Kultur der Risikobewusstheit und -bereitschaft im gesamten Projektteam. Dies ist entscheidend, um die Herausforderungen moderner Cloud-Projekte erfolgreich zu meistern und innovative, sichere und effiziente Cloud-Lösungen zu liefern.
  
### Kompetenzen

* B5.4  Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen  
* B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln 

### Lernziele / Taxonomie 

**B5.4 Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen**

**Lernziel 1:** Verstehen, wie man eine umfassende technische Risikoanalyse durchführt.**

***Praxisanwendung:*** In einem Cloud-Migrationsprojekt eine Risikoanalyse durchführen, um potenzielle Sicherheits-, Leistungs- und Kompatibilitätsprobleme zu identifizieren. Die Ergebnisse dieser Analyse werden verwendet, um präventive Massnahmen in die Projektplanung zu integrieren, wie z.B. zusätzliche Sicherheitstests oder die Auswahl anderer Cloud-Dienste, die besser zu bestehenden Systemen passen.

**Lernziel 2:** Lernen, wie die Ergebnisse der Risikoanalyse effektiv in die Projektplanung integriert werden.

***Praxisanwendung:*** Nach der Identifikation von Risiken bei der Entwicklung einer neuen Softwareanwendung für Cloud-Speicherlösungen, die Ergebnisse nutzen, um kritische Entwicklungsetappen zu definieren, bei denen zusätzliche Ressourcen oder Überprüfungen erforderlich sind, um die Risiken zu minimieren.

**B5.7 Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln**

**Lernziel 3:** Entwicklung von Fähigkeiten zur Erstellung effektiver Ausweichpläne für identifizierte Risiken.

***Praxisanwendung:*** Für ein Projekt zur Implementierung eines neuen IT-Sicherheitsprotokolls, Ausweichpläne entwickeln, die greifen, falls die Integration des Protokolls in bestehende Systeme länger dauert als erwartet oder Kompatibilitätsprobleme auftreten. Dies könnte die vorübergehende Beibehaltung alter Sicherheitssysteme umfassen, bis die Probleme gelöst sind.

**Lernziel 4:** Üben, wie Ausweichpläne in die Kommunikation und Berichterstattung des Projekts integriert werden, um Transparenz und Vorbereitung zu gewährleisten.

***Praxisanwendung:*** Einrichtung eines regelmässigen Update-Meetings für das Projektteam und die Stakeholder, in dem der Status der Umsetzung besprochen und die Ausweichpläne vorgestellt werden, falls bestimmte Risiken Realität werden. Dies stellt sicher, dass alle Beteiligten über potenzielle Probleme und die geplanten Reaktionen informiert sind, wodurch Unsicherheiten reduziert und das Vertrauen in das Projektmanagement gestärkt werden.

### Transfer
* Der Transfer von der Theorie in die Praxis erfolgt durch die Realisierung der Semesterarbeiten.

### Hands-on

<mark>Work in progress</mark>

### Links

[Organisationshandbuch - Risikoanalyse](https://www.orghandbuch.de/Webs/OHB/DE/OrganisationshandbuchNEU/4_MethodenUndTechniken/Methoden_A_bis_Z/Risikoanalyse/risikoanalyse_node.html)

<!---               ## Links

<mark>Work in progress</mark>
-->
