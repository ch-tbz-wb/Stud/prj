# Inhalt der Kompetenz Qualitätsmanagement

[[_TOC_]]

Qualitätsmanagement in Informatikprojekten ist ein essenzieller Bestandteil, um den Erfolg und die Nachhaltigkeit der entwickelten Lösungen sicherzustellen.

## Qualitätsmanagement

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

* A2.3  Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen  
* A2.4  Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
* B5.4  Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen  
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen  
* B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln  
* B5.9  Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen  
* B5.10  ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen  

### Lernziele / Taxonomie 

### A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren

**Lernziel**: Die Fähigkeit erlernen, relevante Informationen effektiv zu filtern und aufzubereiten, um die Entscheidungsfindung zu unterstützen.

**Praxisanwendung**: Bei der Entwicklung eines neuen Cloud-Speicherdienstes die technischen Spezifikationen und Benutzeranforderungen klar definieren und kommunizieren, um sicherzustellen, dass das Entwicklerteam die Informationen erhält, die es benötigt, ohne von irrelevanten Daten überwältigt zu werden.

### A2.4 Arbeitsergebnisse zielgruppenadäquat präsentieren

**Lernziel**: Kompetenzen entwickeln, um Arbeitsergebnisse effektiv und überzeugend verschiedenen Stakeholdern zu präsentieren.

**Praxisanwendung**: Eine Präsentation für das Management erstellen, die den Fortschritt und die ersten Ergebnisse des Cloud-Speicherdienst-Projekts hervorhebt, unter Verwendung von Diagrammen und Fallstudien, um den Nutzen und die Wirksamkeit der Lösung zu unterstreichen.

### B5.4 Eine technische Risikoanalyse durchführen

**Lernziel**: Lernen, wie man systematisch technische Risiken erkennt, bewertet und in die Projektplanung einbezieht.

**Praxisanwendung**: Vor Beginn der Entwicklung des Cloud-Speicherdienstes eine Risikoanalyse durchführen, um potenzielle Sicherheitsprobleme zu identifizieren, und Strategien entwickeln, um diese Risiken zu mindern, wie z.B. die Implementierung zusätzlicher Verschlüsselungsmethoden.

### B5.5 Initiative und Kreativität bei der Projektdurchführung zeigen

**Lernziel**: Die Fähigkeit entwickeln, kreative Lösungen für Projektprobleme zu finden und die Initiative zur Umsetzung dieser Lösungen zu ergreifen.

**Praxisanwendung**: Eine innovative Lösung für die Datenredundanz in einem Cloud-Speichersystem entwerfen, um die Effizienz zu steigern und gleichzeitig die Kosten zu senken.

### B5.7 Ausweichpläne entwickeln

**Lernziel**: Kompetenzen im Entwickeln von Plänen zur Reaktion auf unvorhergesehene Umsetzungsprobleme erwerben.

**Praxisanwendung**: Ein Ausweichplan für das Cloud-Speicherdienst-Projekt, falls kritische Komponenten nicht wie geplant funktionieren, einschliesslich der vorübergehenden Nutzung alternativer Technologien.

### B5.9 Dokumentation zur Überwachung des Projektfortschritts erstellen

**Lernziel**: Die Erstellung und Pflege effektiver Dokumentationssysteme erlernen, um den Überblick über den Projektfortschritt zu behalten.

**Praxisanwendung**: Ein dynamisches Dashboard einrichten, das Echtzeitdaten zur Leistung des Cloud-Speicherdienstes anzeigt, einschliesslich Entwicklungsfortschritt, Budgetverbrauch und Meilensteinerreichung.

### B5.10 ICT-Projekte termin- und budgetgerecht abschliessen

**Lernziel**: Die Fähigkeit erwerben, Projekte innerhalb der vorgegebenen Zeit- und Budgetrahmen erfolgreich zu managen und abzuschliessen.

**Praxisanwendung**: Die Anwendung agiler Methoden zur Sicherstellung, dass der Cloud-Speicherdienst pünktlich und innerhalb des Budgets fertiggestellt wird, durch regelmässige Überprüfungen und Anpassungen des Projektplanes.

### Transfer
* Der Transfer von der Theorie in die Praxis erfolgt durch die Realisierung der Semesterarbeiten.

### Hands-on

<mark>Work in progress</mark>

### Links

[Organisationshandbuch - Qualitätsmanagement](https://www.orghandbuch.de/Webs/OHB/DE/OrganisationshandbuchNEU/3_managementansaetze_u_instrumente/3_9_Qualit%C3%A4tsmanagement/qualitaetsmanagement-node.html)

<!---               [## Unterthema: Erfolgsfaktoren

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*] 

## Links

<mark>Work in progress</mark> -->

