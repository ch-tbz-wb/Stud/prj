# Projektmanagement Grundlagen

[TOC]

## Was ist ein Projekt?

Ein Projekt ist eine temporäre Anstrengung, die darauf ausgerichtet ist, ein definiertes Ziel zu erreichen. Projekte sind in der Regel durch einen festgelegten Beginn und ein Ende gekennzeichnet und umfassen die Durchführung von Aktivitäten, die zum Erreichen des Zieles erforderlich sind. Projekte können in vielen verschiedenen Bereichen stattfinden, wie beispielsweise in der Technologie, im Bauwesen, im Marketing oder in der Kunst.

Ein Projekt kann aus einer Vielzahl von Aufgaben und Aktivitäten bestehen, die in einem spezifischen Zeitrahmen durchgeführt werden müssen. Um ein Projekt erfolgreich abzuschliessen, ist es wichtig, dass es gut geplant und organisiert wird und dass alle Beteiligten eindeutig definierte Rollen und Verantwortlichkeiten haben. Ein Projekt kann von einer Einzelperson oder einem Team durchgeführt werden und es kann auch externe Partner oder Lieferanten beinhalten.

## Eigenschaften von Projekten

Ein Projekt hat im allgemeinen folgende Eigenschaften:

- Es wird einmalig durchgeführt
- Es hat einen relativ grossen Umfang (Wochen, Monate oder Jahre)
- Die Arbeit wird von einem Team erledigt
- Mit der Durchführung wird ein bestimmtes Ziel verfolgt
- Der Bereich, in welchem sich das Projekt abspielt, ist klar abgegrenzt
- Die behandelte Problematik ist neu
- Es ist Risikoreich (v.a. aufgrund der Neuartigkeit)

## Projektmanagement? Was macht das?

Projektmanagement ist der Prozess, der verwendet wird, um ein Projekt von Anfang bis Ende zu planen, zu organisieren und zu überwachen. Es beinhaltet die Verwaltung von Ressourcen, Budgets, Zeitplänen und Risiken, um sicherzustellen, dass ein Projekt erfolgreich abgeschlossen wird.

Projektmanagement beinhaltet auch die Kommunikation mit allen Beteiligten, um sicherzustellen, dass alle auf dem Laufenden sind und dass alle Ziele und Erwartungen klar verstanden werden. Es kann auch dazu dienen, Probleme zu identifizieren und zu lösen, die während des Projekts auftreten können.

Gute Projektmanagementfähigkeiten sind für die erfolgreiche Durchführung von Projekten von entscheidender Bedeutung. Sie umfassen die Fähigkeit, Ziele zu definieren und zu priorisieren, Ressourcen zu verwalten, Probleme zu lösen und effektiv zu kommunizieren. Es gibt auch verschiedene Werkzeuge und Techniken, die im Projektmanagement verwendet werden, wie beispielsweise Gantt-Diagramme und Risikoanalysen.

### Kompetenzen

- B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  

### Lernziele / Taxonomie

- Die Teilnehmenden wissen, was ein Projekt ist
- Die Teilnehmenden kennen die Eigenschaften von Projekten
- Die Teilnehmenden kennen die Aufgaben des Projektmanagements

### Transfer
* Der Transfer von der Theorie in die Praxis erfolgt durch die Realisierung der Semesterarbeiten.

### Hands-on

Im Video [Die Grundlagen des Projektmanagements](https://www.nanoo.tv/link/v/CVJPkTyn) erhalten Sie einen guten Überblick über die Grundlagen des Projektmanagements. Sie finden darin Informationen zu den Eigenschaften von Projekten, dem Projektablauf, dem Projektauftrag, der Projektdokumentation und dem magischen Dreieck.

### Links

- [Wikipedia](https://de.wikipedia.org/wiki/Projekt)

## Erfolgsfaktoren für ein Projekt

Es gibt viele Faktoren, die den Erfolg eines Projekts beeinflussen können. Einige der wichtigsten Erfolgsfaktoren für ein Projekt sind:

**Klare Ziele und Anforderungen**: Es ist wichtig, dass das Projektziel und die Anforderungen klar definiert sind, damit alle Beteiligten wissen, was erwartet wird und welche Ergebnisse erzielt werden sollen.

**Gute Planung**: Eine gründliche Planung ist der Schlüssel zum Erfolg eines Projekts. Dies beinhaltet die Zuweisung von Ressourcen, die Festlegung von Zeitplänen und die Identifizierung von Risiken.

**Effektive Kommunikation**: Es ist wichtig, dass alle Beteiligten gut kommunizieren, um sicherzustellen, dass alle auf dem Laufenden sind und dass es keine Missverständnisse gibt.

**Erfahrene Führung**: Eine erfahrene Führungspersönlichkeit, die in der Lage ist, das Projekt zu leiten und zu motivieren, kann dazu beitragen, dass das Projekt erfolgreich abgeschlossen wird.

**Flexibilität**: Die Fähigkeit, sich an sich ändernde Bedingungen anzupassen, kann dazu beitragen, dass das Projekt erfolgreich abgeschlossen wird.

**Effiziente Ressourcenverwaltung**: Eine effektive Verwaltung von Ressourcen, einschliesslich Budget, Personal und Material, kann dazu beitragen, dass das Projekt im Zeitplan und innerhalb des Budgets bleibt.

**Qualität**: Ein Fokus auf Qualität kann dazu beitragen, dass das Projekt erfolgreich abgeschlossen wird, indem sichergestellt wird, dass alle Arbeiten ordnungsgemäss durchgeführt werden und dass das endgültige Ergebnis den Anforderungen entspricht.

### Kompetenzen

- B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  

### Lernziele / Taxonomie 

- Die Teilnehmenden kennen Faktoren, welche den Erfolg von Projekten begünstigen

### Transfer
* Der Transfer von der Theorie in die Praxis erfolgt durch die Realisierung der Semesterarbeiten.

### Hands-on
* Siehe dieses Repository und das entsprechende MS-Teams.

### Links

- [Wikipedia: Erfolgsfaktoren für Projekte](https://de.wikipedia.org/wiki/Projektmanagement#Erfolgsfaktoren)


## Links

- [Wikipedia: Projektmanagement](https://de.wikipedia.org/wiki/Projektmanagement)
- [Wikipedia: Projekt](https://de.wikipedia.org/wiki/Projekt)

