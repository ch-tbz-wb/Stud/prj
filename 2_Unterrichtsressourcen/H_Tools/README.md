# Inhalt der Kompetenz Tools

[[_TOC_]]

*Ersetzen mit einer allgemeinen Beschreibung mit evtl. vorhandenem Big Picture oder Einführungsfilm*
  
## Unterthema: Eigenschaften von Projekten

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

* A2.3  Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen  
* A2.4  Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  
* B5.9  Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen  
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen  

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Unterthema: Erfolgsfaktoren

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

