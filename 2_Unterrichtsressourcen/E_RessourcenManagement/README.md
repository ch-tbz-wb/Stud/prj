# Inhalt der Kompetenz Ressourcenmanagement

[[_TOC_]]

*Ersetzen mit einer allgemeinen Beschreibung mit evtl. vorhandenem Big Picture oder Einführungsfilm*

 
## Unterthema: Eigenschaften von Projekten

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  
* B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln  
* B5.8  Verhältnis zwischen Kosten und Terminen nach Vorgaben optimieren  
* B5.10  ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen  
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen  
* B13.3  Services gemäss Pflichtenheft planen  

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Unterthema: Erfolgsfaktoren

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

 * [Wirtschaftlichkeitsrechnung-und-Investitionsrechnung](https://www.business-wissen.de/hb/wirtschaftlichkeitsrechnung-und-investitionsrechnung/)

## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

