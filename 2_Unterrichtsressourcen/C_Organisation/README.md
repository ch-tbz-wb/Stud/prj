# Inhalt der Kompetenz Projektorganisation

[[_TOC_]]

In der dynamischen Welt der Informationstechnologie, insbesondere im Bereich der Cloud-Technologien, stehen Organisationen vor der Herausforderung, innovative Lösungen schnell und effizient zu entwickeln und bereitzustellen. Cloud Native Engineering spielt dabei eine zentrale Rolle, da es Unternehmen ermöglicht, skalierbare und resiliente Anwendungen zu erstellen, die die Cloud-Infrastruktur optimal nutzen. Die in diesem Modul thematisierten Kompetenzen  bilden das Fundament für Fachkräfte, die in diesem schnelllebigen Umfeld nicht nur überleben, sondern auch erfolgreich sein möchten.

Betrachten wir ein konkretes Praxisbeispiel aus dem Bereich Cloud Native Engineering: die Implementierung eines Cloud-basierten, skalierbaren Microservice-Architektursystems für ein E-Commerce-Unternehmen. Das Unternehmen, ein mittelständischer Online-Händler, sah sich mit wachsenden Herausforderungen konfrontiert: Die bestehende monolithische Plattform konnte mit dem rapiden Wachstum der Nutzerzahlen und dem steigenden Datenaufkommen nicht mehr mithalten. Die Entscheidung fiel auf eine Migration zu einer Microservice-Architektur, die in der Cloud gehostet werden sollte, um Flexibilität, Skalierbarkeit und eine verbesserte Kundenexperience zu gewährleisten. Als Projektleiter sind Sie nun gefordert, diesen bedeutenden Wechsel interdisziplinär zu managen, indem Sie eng mit verschiedenen Teams zusammenarbeiten und die Kommunikation zwischen allen beteiligten Stakeholdern sicherstellen.
  
## Unterthema: Eigenschaften von Projekten

In der heutigen Ära der Digitalisierung, angetrieben durch fortschrittliche Technologien wie Cloud Computing, Big Data und künstliche Intelligenz, steigen die Anforderungen an Unternehmen, ihre IT-Infrastrukturen kontinuierlich anzupassen und zu verbessern. Diese Technologien ermöglichen eine schnellere Datenverarbeitung, skalierbarkeit, verbesserte Analysefähigkeiten und innovative Geschäftsmodelle, setzen Unternehmen jedoch auch neuen Sicherheitsrisiken aus. Angesichts dieser Herausforderungen sind die in diesem Modul thematisierten Kompetenzen essentiell, um Projekte erfolgreich zu leiten und zu realisieren. Durch die Vertiefung dieser Kompetenzen sind Sie in der Lage, IT-Projekte nicht nur technisch umzusetzen, sondern auch die Akzeptanz und effektive Nutzung der neuen Systeme im Unternehmen sicherzustellen.

### Kompetenzen

* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen  
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen  
* B6.3  Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen  

### Lernziele / Taxonomie 

**B5.1 – Eigenständige Planung und Steuerung von ICT-Projekten:** In Ihrer Rolle als Projektleiter beginnt die sorgfältige Planung des Projekts mit einer strukturierten Erhebungsphase, in der Sie den aktuellen Stand der Sicherheitstechnologien und -protokolle innerhalb des Unternehmens erfassen. Anschliessend führen Sie eine detaillierte Analyse durch, um Sicherheitslücken und Verbesserungspotenziale zu identifizieren. Die gewonnenen Erkenntnisse werden gewürdigt, um die Dringlichkeit und Relevanz der verschiedenen Bedürfnisse zu bestimmen. Basierend auf dieser Bewertung entwickeln Sie mehrere Lösungsvarianten.
Diese Lösungsvarianten werden einer gründlichen Bewertung unterzogen, um ihre Machbarkeit, Kosten und Auswirkungen auf das Unternehmen zu analysieren. Nach dieser Bewertung treffen Sie eine fundierte Auswahl der am besten geeigneten Lösung. Wichtig dabei ist, die Systemgrenzen präzise zu definieren, um den Umfang des Projekts klar abzugrenzen und sicherzustellen, dass alle Projektbeteiligten ein gemeinsames Verständnis der Projektziele haben.
Mit der ausgewählten Lösung gehen Sie in die Umsetzungsphase über, während der Sie einen detaillierten Zeitplan ausarberiten und die geeignete Projektmanagementmethode für die Implementierung bestimmen. Sie steuern das Projektteam durch die verschiedenen Phasen, überwachen den Fortschritt und passen den Plan bei Bedarf an, um sicherzustellen, dass das Projekt termingerecht und im Rahmen des Budgets abgeschlossen wird.

**B5.5 – Initiative, Kreativität und Durchsetzungsvermögen:** Bei der Planung und beim Rollout des Systems könnten Sie auf technische und organisatorische Hindernisse stossen, etwa bei der Integration in bestehende IT-Systeme oder wenn Personelle veränderungen umgesetzt werden müssen. Ihre Fähigkeit, kreative Lösungen zu entwickeln und das Team sowie das Management für diese zu gewinnen, ist hier gefragt. Ein starkes Durchsetzungsvermögen, die Ausarbeitung und Visualisierung von Fakten und Ihre Transparenz bei der Palnung hilft Ihnen, das Projekt trotz möglicher Widerstände erfolgreich voranzutreiben.

**B6.2 – Ressourcenplanung und Budgetierung:** Die Implementierung eines neuen Systems erfordert eine akkurate Budgetierung und sorgfältige Ressourcenplanung. Darüber hinaus ist es wichtig, den Einsatz des Projektteams effektiv zu organisieren und die Kommunikation zwischen allen Beteiligten sicherzustellen.

**B6.3 – Kommunikation mit Stakeholdern:** Die transparente Kommunikation mit allen Stakeholdern ist entscheidend, um Unterstützung für das Projekt zu gewinnen und den Informationsbedarf der verschiedenen Interessengruppen zu decken. Regelmässige Projektupdates und das aktive Einholen von Feedback tragen dazu bei, Missverständnisse zu vermeiden und die Akzeptanz des neuen Sicherheitssystems im Unternehmen zu fördern.

### Transfer
Im Rahmen der Semesterarbeit.

### Hands-on
Siehe Unterverzeichnis Hands-on.

### Links
<mark>Work in progress...</mark>


## Unterthema: Erfolgsfaktoren

### Kompetenzen

* A1.3  Fachliche Kenntnisse kombiniert mit betriebswirtschaftlichem Wissen für einen ökonomisch, ökologisch und sozial erfoglreichen Geschäftsgang einsetzen.

* B5.3  Die Erfolgsfaktoren, die Zusammenarbeit im Team, die Planung der Ressourcen, die Umweltbelastung und die Kostenkontrolle berücksichtigen.

* B5.4  Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen.

### Lernziele / Taxonomie 

**Klare Zieldefinition (SMART):** Ziele müssen spezifisch, messbar, erreichbar, relevant und zeitgebunden formuliert werden. Zum Beispiel sollte das Ziel nicht nur die Implementierung eines Sicherheitssystems beinhalten, sondern auch spezifizieren, dass die Systemeinführung zu einer Reduktion der Sicherheitsvorfälle um 25% innerhalb eines Jahres führen soll. Dies ermöglicht eine klare Erfolgsmessung und stellt sicher, dass alle Beteiligten auf ein konkretes Ergebnis hinarbeiten.

**Strukturierte Projektplanung:** Die Planung sollte klare Phasen, Verantwortlichkeiten durch Rollendefinitionen und Meilensteine umfassen, die mit den SMART-Zielen übereinstimmen. Zum Beispiel sollte jede Phase des Projekts einen festgelegten Abschlusszeitpunkt und spezifische Outputs haben, die messbar sind und deren Erreichung überprüft werden kann.

**Effektives Stakeholder-Management:** Identifizieren Sie alle relevanten Stakeholder und ermitteln Sie deren spezifische Erwartungen und Anforderungen an das Projekt. Erstellen Sie einen Plan, wie und wann diese Stakeholder kommunikativ eingebunden werden, um ihre fortlaufende Unterstützung und Zufriedenheit sicherzustellen.

**Kompetentes Projektteam:** Definieren Sie konkret die benötigten Kompetenzen und Erfahrungen für das Projektteam. Legen Sie fest, wie die Kompetenzen im Team verteilt sind und planen Sie gegebenenfalls Schulungen, um spezifische Lücken zu schliessen.

**Agile Methodik:** Legen Sie fest, welche agilen Praktiken angewendet werden (z.B. Scrum oder Kanban), und definieren Sie spezifische Kriterien für ihre Anwendung, wie Sprint-Längen oder die Definition von "Done" für Tasks und Features.

**Kommunikation:** Erarbeiten Sie einen Kommunikationsplan, der festlegt, wie oft, in welcher Form und über welche Kanäle kommuniziert wird. Setzen Sie messbare Ziele für die Kommunikationseffektivität, zum Beispiel durch regelmässige Zufriedenheitsumfragen unter den Stakeholdern.

**Risikomanagement:** Identifizieren Sie potenzielle Risiken und bewerten Sie diese nach Eintrittswahrscheinlichkeit und Auswirkungen. Entwickeln Sie für jedes Risiko spezifische Gegenmassnahmen und definieren Sie Indikatoren, die eine frühzeitige Erkennung ermöglichen.

**Qualitätssicherung:** Definieren Sie klare Qualitätskriterien für alle Projektoutputs und legen Sie fest, wie diese durch Tests und Reviews überprüft werden. Setzen Sie messbare Standards, zum Beispiel in Form von Fehlerquoten oder Nutzerzufriedenheit.

**Change Management:** Entwickeln Sie einen Plan für das Change Management, der festlegt, wie Veränderungen kommuniziert, implementiert und deren Akzeptanz gemessen wird. Definieren Sie klare KPIs zur Bewertung der Effektivität der Change-Management-Massnahmen.

**Nachhaltige Implementierung und Support:** Planen Sie für die Zeit nach der Projektimplementierung, indem Sie Kriterien für den laufenden Support und die Wartung des Systems festlegen. Definieren Sie Service-Level-Agreements (SLAs) und messbare Ziele für die Nutzerunterstützung, um die langfristige Funktionalität und Zufriedenheit sicherzustellen.

### Transfer
Im Rahmen der Semesterarbeit.

### Hands-on

<mark>Work in progress...</mark>

### Links

<mark>Work in progress...</mark>


### Links

<mark>Work in progress...</mark>

## Links

<mark>Work in progress...</mark>

