# Inhalt der Kompetenz A

[TOC]

## Wozu dienen Vorgehensmodelle?

Vorgehensmodelle beim Projektmanagement dienen dazu, eine strukturierte und systematische Methode für die Planung, Durchführung und Überwachung von Projekten bereitzustellen. Sie bieten eine Rahmenstruktur, die dabei hilft, das Projekt erfolgreich abzuschliessen und die Erwartungen der Stakeholder zu erfüllen.

Vorgehensmodelle können beispielsweise dazu dienen:

**Den Projektfortschritt zu planen und zu überwachen**: Vorgehensmodelle liefern klare Schritte und Meilensteine, die dabei helfen, das Projekt auf Kurs zu halten und sicherzustellen, dass alle wichtigen Aufgaben erledigt werden.

**Risiken zu identifizieren und zu minimieren**: Vorgehensmodelle bieten eine systematische Methode, um mögliche Risiken zu identifizieren und Massnahmen zu ergreifen, um diese zu minimieren.

**Kommunikation und Zusammenarbeit zu verbessern**: Vorgehensmodelle helfen dabei, die Kommunikation und Zusammenarbeit innerhalb des Projektteams und mit den Stakeholdern zu verbessern, indem sie klare Rollen und Verantwortlichkeiten festlegen.

**Projekterfolg zu messen**: Vorgehensmodelle liefern klare Leistungskriterien, an denen der Erfolg des Projekts gemessen werden kann.

Insgesamt tragen Vorgehensmodelle dazu bei, das Projektmanagement zu vereinfachen und die Wahrscheinlichkeit von Projekterfolg zu erhöhen.

### Kompetenzen

* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern  
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen  
* B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln  

### Lernziele / Taxonomie

- Die Teilnehmenden können ein Projekt in Phasen unterteilen
- Die Teilnehmenden können für ein Projekt eine sinnvolles Vorgehen vorschlagen und begründen

[### Transfer]::

[### Hands-on]::

### Links

Beim Projektmanagement kennt man drei Vorgehenstypen:

- [planbasierte Projektmanagement](#planbasiertes-projektmanagement)
- [agiles Projektmanagement](#agiles-projektmanagement)
- hybrides Projektmanagement

## Planbasiertes Projektmanagement

Im planbasierten Projektmanagement wird ein detaillierter Projektplan erstellt, der die Schritte, die Ressourcen und den Zeitrahmen für das Projekt festlegt. Das Ziel des planbasierten Projektmanagements ist es, die Projektziele innerhalb des vorgegebenen Zeit- und Ressourcenrahmens zu erreichen.

Der Projektplan dient als Leitfaden für das Projektmanagement und hilft, die Projektfortschritte zu verfolgen und die Ressourcen effektiv zu nutzen. Er wird in der Regel in Phasen unterteilt, die jeweils spezifische Ziele und Aktivitäten umfassen.

Das planbasierte Projektmanagement ist besonders geeignet für Projekte mit einem hohen Grad an Planungsbedarf und Risiko, wie zum Beispiel bei der Entwicklung neuer Produkte oder bei komplexen IT-Projekten. Es ermöglicht es, die Projektrisiken frühzeitig zu erkennen und zu minimieren und sicherzustellen, dass das Projekt innerhalb des vorgegebenen Zeit- und Ressourcenrahmens abgeschlossen wird.

### Kompetenzen

* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern (KN:3)  
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen (KN:3)
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen (KN:3)

### Lernziele / Taxonomie 

- Die Teilnehmenden kennen planbasierte Vorgehensmodelle und deren Einsatz begründen

### Transfer
* Der Transfer von der Theorie in die Praxis erfolgt durch die Realisierung der Semesterarbeiten.

### Hands-on
[Hands-on_Vorgehensmodelle](https://gitlab.com/ch-tbz-wb/Stud/prj/-/blob/main/2_Unterrichtsressourcen/B_Vorgehensmodelle/Hands-on_Vorgehensmodelle.md)

### Links

Beispiele für Vorgehensmodelle beim planbasierten Projektmanagement:

- [Wasserfallmodell](https://de.wikipedia.org/wiki/Wasserfallmodell)
- [RUP-Modell](https://de.wikipedia.org/wiki/Rational_Unified_Process)
- [Prince2](https://de.wikipedia.org/wiki/PRINCE2)


## Agiles Projektmanagement

Das agile Projektmanagement ist ein Ansatzbasiertes Projektmanagement, bei dem sich die Planung an den tatsächlichen Fortschritten des Projekts orientiert. Im Gegensatz zum planbasierten Projektmanagement, bei dem zu Beginn eines Projekts ein detaillierter Plan erstellt wird, der die Schritte, Ressourcen und den Zeitrahmen festlegt, wird im agilen Projektmanagement ein allgemeiner Rahmen festgelegt, innerhalb dessen sich das Projekt entwickelt.

Das agile Projektmanagement setzt auf flexible und iterative Arbeitsmethoden und fördert die Zusammenarbeit und Kommunikation innerhalb des Projektteams. Es konzentriert sich auf die kontinuierliche Lieferung von Werten und zielt darauf ab, schnell auf Veränderungen reagieren zu können.

Das agile Projektmanagement ist besonders geeignet für Projekte, bei denen es wichtig ist, schnell auf Veränderungen reagieren zu können, wie zum Beispiel bei der Entwicklung von Software. Es ermöglicht es, schnell Ergebnisse zu liefern und das Projekt an die sich ändernden Anforderungen anzupassen.

### Kompetenzen

* B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern (KN:3)  
* B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen (KN:3)
* B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen (KN:3)

### Lernziele / Taxonomie 

- Die Teilnehmenden kennen agile Vorgehensmodelle und können deren Einsatz begründen

[### Transfer]::

[### Hands-on]::

[### Links]::

Verbreitete agile Vorgehensmodelle sind:

- [Scrum (Wikipedia)](https://de.wikipedia.org/wiki/Scrum)
- [Kanban (Wikipedia)](https://de.wikipedia.org/wiki/Kanban)

## Links

- [Wahl der Vorgehensweise (Wikipedia)](https://de.wikipedia.org/wiki/Projektmanagement#Wahl_der_Vorgehensweise)
