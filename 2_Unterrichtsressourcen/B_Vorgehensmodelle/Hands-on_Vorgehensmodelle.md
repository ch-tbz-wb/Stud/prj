# Übersicht der Vorgehensmodelle mit Ressourcen

## Allgemeine Übersicht:  

* https://www.lucidchart.com/blog/de/projektmanagement-methoden-im-vergleich 

* https://omr.com/de/reviews/contenthub/projektmanagement-methoden 

* https://monday.com/blog/de/projektmanagement/projektmanagement-methoden/ 

* https://asana.com/de/resources/project-management-methodologies

## Auflistung der einzelnen Methoden

| Gruppe | Methode               | Bemerkungen                                                                                                                                 |
|--------|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| 1      | Wasserfall / Prince2  | [Lucidchart über Wasserfallmethode](https://www.lucidchart.com/blog/waterfall-project-management-methodology), [Projekte leicht gemacht](https://projekte-leicht-gemacht.de/blog/projektmanagement/klassisch/wasserfallmodel) |
| 2      | Scrum                 | Tools: [GoodDay](https://www.goodday.work/), [OpenSourceScrum](https://www.opensourcescrum.com/), [Grid Dynamics](https://www.griddynamics.com/services/global-team/blog/development-team/free-agile-project-management-tools-for-your-scrum) |
| 3      | Kanban / Scrumban     | Tool: [ClickUp](https://app.clickup.com/), Ressource: [Atlassian über Scrumban](https://www.atlassian.com/de/agile/project-management/scrumban) |
| 4      | Hermes                | Ressourcen: [Hermes ZH](https://hermes.zh.ch/), [Hermes Admin](https://www.hermes.admin.ch/)                                                 |
| 5      | Lean                  | Ressource: [Asana über Lean Projektmanagement](https://asana.com/de/resources/lean-project-management)                                        |
| 6      | Projektstrukturplan   | Ressource: [Projekte leicht gemacht](https://projekte-leicht-gemacht.de/blog/projektmanagement/klassisch/projektplanung/projektstrukturplan/), [Bildungsbibel über PSP](https://management.bildungsbibel.de/projektstrukturplan-psp) |
