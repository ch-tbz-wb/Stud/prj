# Unterrichtsressourcen

Das Modul behandelt folgende Themenbereiche:

* Unterrichtsressource A: [Grundlagen Projektmanagement](A_Grundlagen/)
  * Eigenschaften von Projekten
  * Gründe für Projekte
  * Erfolgsfaktoren
  * Projektziele
  * Lebenszyklus
* Unterrichtsressource B: [Vorgehensmodelle](B_Vorgehensmodelle/)
  * Traditionelle, sequentielle Vorgehensmodelle
  * Agile Vorgehensmodelle
  * Vorgehensmodelle für Change Management
  * Prozessbasierte Vorgehensmodelle
  * Andere Vorgehensmodelle
* Unterrichtsressource C: [Projektorganisation](C_Organisation)
  * Rollen
  * Führung/Leadership
  * Anforderung an Projektleitung
  * Teambildung
* Unterrichtsressource D: [Projektkommunikation](D_Kommunikation)
  * Stakeholder
  * Projektdokumentation
  * Projektmarketing
* Unterrichtsressource E: [Ressourcenmanagement](E_RessourcenManagement)
  * Mitarbeitende
  * Infrastruktur und Mobiliar
  * Geräte und Material
  * Finanzen / Kosten
* Unterrichtsressource F: [Risikomanagement](F_RisikoManagement)
* Unterrichtsressource G: [Qualitätsmanagement](G_Qualit%C3%A4tsManagement)
* Unterrichtsressource H: [Work Management Tools](H_Tools)
  
## Auftrag

- [Kennen einer Projekt-Managementtechnik und diese anwenden können](2_Unterrichtsressourcen/A)
- [Führen eines Projektes in Einzelarbeit](2_Unterrichtsressourcen/B)
- Verstehend und Anwenden der einzelnen Projektphasen und  der einzelnen Schritte im Projektzyklus (3)
- Projktabgrenzung visualisieren
- sowie der diversen Techniken für die Teilschritte im Projekt wie z.B. Ziele Setzen (mit Hilfe von Erhebungstechniken) sowie Lösungsvarianten definieren und bewerten
- Erfahrung sammeln in der realen Terminplanung für das Projekt inkl. Erarbeitung des Masterplans
- und Soll-/Ist-Vergleich von Zeitaufwand und ev. Kosten bei der Projektdurchführung (2)

