# Zertifizierungen

## Externe Anbieter
* [IPMA (International Project Management Association)](https://www.vzpm.ch/de/zertifizierungen/zertifizierung-von-personen/zertifizierungssystem-ipma)
* [Prince2](https://www.digicomp.ch/weiterbildung-service-projektmanagement/prince2)
* [Hermes](https://www.bk.admin.ch/bk/de/home/digitale-transformation-ikt-lenkung/hermes_projektmanagementmethode.html)
[Scrum Grundlagen](https://www.bfh.ch/wirtschaft/de/weiterbildung/fachkurse/scrum-grundlagen/)
* [Übersicht _Internationale Zertifizierung im Projektmanagement_](https://www.sgo.ch/projektmanagement/internationale-zertifizierung.html?msclkid=02747bbfe45d18ff94561e638fa47032)

