# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen 

### PRJ1
 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen  (KN:3)
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren (KN:3)

### PRJ2
 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren
   * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen
 * B5 ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren 
    * B5.1  ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern (KN:3)  
    * B5.4  Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen (KN:3)
    * B5.5  Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen (KN:3)
    * B5.7  Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln (KN:3)
    * B5.8  Verhältnis zwischen Kosten und Terminen nach Vorgaben optimieren (KN:2)
    * B5.9  Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen (KN:3)
    * B5.10  ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen (KN:3)

### PRJ3
 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren
   * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen
 * B6 Eine ICT-Organisationseinheit leiten
    * B6.2  Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen
    * B6.3  Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen

### PRJ4
 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren
   * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen
 * B6 Eine ICT-Organisationseinheit leiten
    * B6.3  Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen

### PRJ5
 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren
   * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen
 * B6 Eine ICT-Organisationseinheit leiten
    * B6.3  Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen
  * B14 Konzepte und Services umsetzen
    * B14.4 Risiken beim Betrieb von ICT-Systemen systematisch erheben und Massnahmen ableiten


## Modulübergreifende Differenzierung der PRJ-Module
Die Module PRJ1 bis PRJ5 bauen systematisch aufeinander auf und fokussieren sich jeweils auf spezifische Handlungskompetenzen, die den Studierenden sukzessive von den Grundlagen bis hin zu anspruchsvollen Projektszenarien begleiten. Nachfolgend eine Übersicht der zentralen Unterschiede.

### PRJ1: Einstieg ins Projektmanagement
Das erste Modul legt den Schwerpunkt auf die Grundlagen des Projektmanagements. Die Studierenden lernen effektive Kommunikationsstrategien sowie grundlegende Projektmanagement-Tools kennen. Der Fokus liegt auf:
- Vermittlung der Basiskompetenzen für sachlogische, transparente Kommunikation.
- Einführung in agile Methoden und Planungstools.
- Aufbau einer starken Teamdynamik und Sozialkompetenz.

### PRJ2: Vertiefung und erste Projekterfahrungen
Im zweiten Modul wenden die Studierenden das Gelernte aus PRJ1 in ersten praxisnahen Projekten an. Es werden komplexere Themen wie Risikomanagement und Projektsteuerung eingeführt:
- Erstellung von Risikoanalysen und Entwicklung von Fallback-Plänen.
- Planung und Steuerung von ICT-Projekten bis zur Ausführungsreife.
- Einführung in Budget- und Terminmanagement.

### PRJ3: Leitung einer ICT-Organisationseinheit
PRJ3 erweitert die Kompetenzen der Studierenden, indem der Fokus auf die Leitung von ICT-Organisationseinheiten gelegt wird:
- Planung und Verwaltung von Ressourcen sowie Stakeholder-Kommunikation.
- Anwendung moderner Entscheidungsunterstützungstools.
- Aufbau und Leitung von Teams in komplexen organisatorischen Strukturen.

### PRJ4: Strategisches Management und Kommunikation
Im vierten Modul wird die strategische Perspektive verstärkt:
- Entwicklung langfristiger Strategien zur Effizienzsteigerung von ICT-Organisationseinheiten.
- Professionelle Kommunikation auf Managementebene.
- Einführung in Szenario-Analysen und strategisches Risikomanagement.

### PRJ5: Abschluss und Integration
Das fünfte Modul rundet die Ausbildung ab, indem die Studierenden ein komplexes ICT-Projekt abschliessen:
- Analyse und Optimierung von organisatorischen Prozessen.
- Integration neuer Technologien und Entwicklung nachhaltiger Lösungen.
- Professionelle Dokumentation, Präsentation und Übergabe von Projekten.
- Vertiefung der Fähigkeiten im Krisen- und Change-Management.

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledi-gen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.
