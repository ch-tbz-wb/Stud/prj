![TBZ Logo](./x_gitressourcen/tbz_logo.png)  

# PRJ - Projektmanagement

## Kurzbeschreibung des Moduls

Dipl. Informatikerinnen HF/dipl. Informatiker HF organisieren komplexe Vorhaben und Projekte, einschliesslich aller Interaktionen mit allen Stakeholdern. Sie üben Einfluss auf die Projektstrategie durch Vorschläge mit neuen oder alternativen Lösungen aus und gleichen zwischen Effizienz- und Effektivitätsanforderungen aus. Dipl. Informatikerinnen HF/dipl. In-formatiker HF übernehmen die Gesamtverantwortung für Projektergebnisse, inklusive dem Finanz- und Ressourcenmanagement.

Das Modul *Projektmanagement* soll den Teilnehmern dabei helfen, die Fähigkeiten und Kenntnisse zu entwickeln, die erforderlich sind, um erfolgreich Projekte zu planen, zu organisieren und zu überwachen. Dazu gehören folgende Lernziele:

**Verstehen der Grundlagen des Projektmanagements**: Dazu gehören die Definition von Projekten, die verschiedenen Phasen des Projektlebenszyklus und die wichtigsten Konzepte und Terminologie im Projektmanagement.

**Erlernen von Techniken zur Projektplanung und -organisation**: Dazu gehören die Erstellung von Zeitplänen und Ressourcenplänen, die Zuweisung von Verantwortlichkeiten und die Identifizierung von Risiken.

**Lernen, wie man Projekte effektiv überwacht und kontrolliert**: Dazu gehört das Überwachen von Fortschritt und Leistung, das Lösen von Problemen und das Anpassen von Plänen, wenn sich die Umstände ändern.

**Verstehen der Rolle der Kommunikation im Projektmanagement**: Dazu gehört das effektive Kommunizieren von Zielen, Erwartungen und Fortschritt an alle Beteiligten.

**Lernen, wie man Projekte erfolgreich abschliesst**: Dazu gehört das Überprüfen des endgültigen Ergebnisses und das Dokumentieren von Erfahrungen und Lektionen, die aus dem Projekt gezogen werden können.

**Verstehen der verschiedenen Werkzeuge und Techniken**, die im Projektmanagement verwendet werden: Dazu gehören beispielsweise Gantt-Diagramme, Risikoanalysen und Earned Value-Analysen.

**Entwickeln von Führungsfähigkeiten im Projektmanagement**: Dazu gehört das Führen von Teams, das Motivieren von Mitarbeitern und das Entwickeln von Fähigkeiten zur Problemlösung und Entscheidungsfindung.

## Angaben zum Transfer der erworbenen Kompetenzen

- Umsetzung der erlernten Theorie in Einzel- und Gruppenarbeiten und vor allem bei einer Semesterarbeit.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

- Keine

### Nachfolgende Module

- PRJ-DevOps im 4. Semester

## Dispensation

- Projektmanagement: Wenn vorab schon eine Ausbildung zum Projektmanager erfolgte (vielfältige Zertifikate am Markt angeboten)
- Entbindet jedoch nicht von der Semesterarbeit.

## Aufbau der Unterlagen zum Modul

### Organisatorisches

[Organisatorisches](0_Organisatorisches) zur Autorenschaft dieser Dokumente zum Modul

### Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzungsvorschlag](3_Umsetzung) mit möglichem Modulablauf

### Fragekatalog

[Fragekatalog](4_Fragekatalog) zu den einzelnen Kompetenzen der Kompetenzmatrix

### Handlungssituationen

[Handlungssituationen](5_Handlungssituationen) mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

---
