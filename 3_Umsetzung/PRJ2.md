# PRJ2 - Projektmanagement 2
 - Bereich: ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren 
 - Semester: 2

## Lektionen
 * Präsenzunterricht: 10
 * Fernunterricht: 30
 * Selbststudium: 20

 ## Fahrplan für PRJ2
- **Fokus**: Technische Planung und Risikoanalyse
- **Ziele**:
  - ICT-Projekte bis zur Ausführungsreife planen.
  - Technische Risiken identifizieren und in die Planung integrieren.
  - Dokumentation und Projektüberwachung.
- **Technik**: Vertiefung von Projektmanagement-Tools wie GitLab.
- **Praxis**: Semesterarbeit mit Risikoanalysen und Ausweichplänen.
- **Rahmenlehrplan-Verbindung**: A2.5 - A2.7, B5.1 - B5.10.

## Lernziele
Im Rahmen des zweiten Semesters im Fach Projektmanagement setzen wir Lernziele, die auf die Projektsteuerung ausgerichtet sind. Der Unterricht ist darauf abgestimmt, nicht nur theoretisches Wissen, sondern auch praktische Fähigkeiten zu vermitteln.

**Lernziel 1: Planung und Steuerung von ICT-Projekten**
Die Studierenden sollen in der Lage sein, ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife zu planen und zu steuern. Dies beinhaltet die Fähigkeit, Projekte von der Konzeption bis zur Fertigstellung zu managen, unter Berücksichtigung aller relevanten Aspekte wie Zeitrahmen, Ressourcen, Kosten und Qualitätsanforderungen.

_Praktische Umsetzung:_ Durchführung von Projektplanungsübungen, in denen Studierende ICT-Projekte konzipieren und Planungsstrategien entwickeln.
Anwendung von Projektmanagement-Tools zur Unterstützung der Planung und Steuerung.

**Lernziel 2: Technische Risikoanalyse**
Studierende führen eigenständig technische Risikoanalysen durch und integrieren die Ergebnisse in die Projektplanung. Dieses Ziel zielt darauf ab, das Bewusstsein und Verständnis für potenzielle Risiken in ICT-Projekten zu schärfen und wie diese Risiken effektiv minimiert werden können.

_Praktische Umsetzung:_ Durchführung von Risikoanalyse im Rahmen der Semesterarbeit.
Erstellung von Risikomanagementplänen, die zeigen, wie identifizierte Risiken in der Projektplanung berücksichtigt werden können.

**Lernziel 3: Initiative, Kreativität und Durchsetzungsvermögen**
Die Studierenden zeigen Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei deren Durchführung. Sie sollen lernen, innovative Lösungen für Projektprobleme zu finden und die notwendigen Schritte zur erfolgreichen Umsetzung ihrer Projekte zu unternehmen.

_Praktische Umsetzung:_ Entwicklung und Präsentation innovativer Projektideen.

**Lernziel 4: Entwicklung von Ausweichplänen**
Studierende entwickeln Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren. Dieses Ziel bereitet die Studierenden darauf vor, proaktiv Lösungen für unerwartete Herausforderungen in ICT-Projekten zu finden.

_Praktische Umsetzung:_ Im Rahmen der Umsetzung von der Semesterarbeit.

**Lernziel 5: Optimierung des Verhältnisses zwischen Kosten und Terminen**
Die Studierenden optimieren das Verhältnis zwischen Wirtschaftlichkeit und Terminen nach Vorgaben. Sie erwerben Kenntnisse in der effizienten Ressourcenallokation und Zeitmanagement, um Projekte innerhalb des Budgets und des Zeitplans erfolgreich abzuschliessen.

_Praktische Umsetzung:_ Übungen zur Kosten-Nutzen-Analyse und zum Zeitmanagement, bei denen die Studierenden praktische Erfahrungen im Umgang mit begrenzten Ressourcen sammeln.

**Lernziel 6: Dokumentation und Überwachung des Projektfortschritts**
Studierende erstellen und pflegen Dokumente, die die Überwachung des Projektfortschritts erleichtern. Sie lernen die Bedeutung der Dokumentation für die transparente Kommunikation und das effektive Management von ICT-Projekten.

_Praktische Umsetzung:_ Anleitung zur Erstellung von Fortschrittsberichten, Statusupdates und abschliessenden Projektberichten.

**Lernziel 7: Termin- und budgetgerechter Projektabschluss**
Die Studierenden schliessen ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen ab. Sie demonstrieren ihre Fähigkeit, Projekte effizient zu managen und die gesetzten Ziele zu erreichen.

_Praktische Umsetzung:_ Abschlussprojekte, in denen die Studierenden ein ICT-Projekt von der Planung bis zur Ausführung durchführen, unter Berücksichtigung aller Lernziele des Kurses.

## Voraussetzungen

 PRJ1 erfolgreich absolviert.

## Dispensation
siehe PRJ1

## Technik

 GitHub, GitLab, Markdown, Projektmanagementspezifische Tools, Repository

## Methoden

 Blended Learning, Einzelarbeit, Flipped Classroom, Frontalunterricht, Gruppenarbeit, Microlearning, Peer Teaching, Projektbasiertes Lernen

## Schlüsselbegriffe

**Ausweichplanung:** Ausweichpläne entwickeln, Potenzielle Umsetzungsprobleme, Reaktion auf Herausforderungen

**Dokumentation und Überwachung:** Dokumente pflegen, Projektfortschritt überwachen, Projektfortschrittsdokumentation

**Initiative und Kreativität:** Durchsetzungsvermögen, Gestaltung von Projekten, Initiative, Kreativität

**Kosten- und Terminmanagement:** Kostenoptimierung, Terminoptimierung, Verhältnis zwischen Kosten und Terminen

**Planung und Steuerung:** ICT-Projekte planen, Planung und Steuerung, Projekte bis zur Ausführungsreife bringen

**Projektabschluss:** Budgetgerechter Abschluss, ICT-Projekte abschliessen, Termin- und budgetgerechter Abschluss, Übereinstimmung mit den Anforderungen

**Risikoanalyse:** Risikoanalyse durchführen, Technische Risikoanalyse, Ergebnisse in der Planung berücksichtigen

## Lehr- und Lernformen

 Einzel- und Gruppenarbeiten, Lehrbuchzusammenfassungen, Lehrgespräche, Lehrervorträge, Lernfragen, Präsentationen, Semesterarbeit als Gruppenarbeit

## Lehrmittel

 * [Das Agile Manifest](https://agilemanifesto.org/)
 * [HERMES (Kanton ZH)](https://hermes.zh.ch)

