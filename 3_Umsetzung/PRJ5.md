# PRJ5 - Projektmanagement 5
 - Bereich: Eine ICT-Organisationseinheit leiten
 - Semester: 5

## Lektionen
 * Präsenz: 10
 * Virtuell: 30
 * Selbststudium: 20

## Fahrplan für PRJ5
| Lektionen | Selbststudium | Inhalt                                                                                 | RP    | Tools                                |
|-----------|---------------|----------------------------------------------------------------------------------------|-------|--------------------------------------|
| 10        | 6             | **Projektabschluss und Optimierung organisatorischer Prozesse**                       | B14.4 | Jira, Confluence, Präsentationssoftware |
| 5         | 4             | - Analyse und Optimierung bestehender Prozesse                                        | B14.4 | RiskWatch, Analyse-Tools             |
| 5         | 4             | - Integration und Bewertung neuer Technologien                                        | B14.4 | Power BI, Jira                       |
| 10        | 6             | **Strategisches Risikomanagement**                                                    | B14.4 | Risikoanalyse-Tools, RiskWatch       |
| 5         | 3             | - Durchführung von Risiko- und Chancenbewertungen                                     | B14.4 | Szenario-Tools, Analyse-Software     |
| 5         | 3             | - Entwicklung nachhaltiger Lösungen für wiederkehrende Herausforderungen              | B14.4 | Simulationssoftware                  |
| 10        | 6             | **Konfliktmanagement und Stakeholder-Kommunikation**                                  | B6.3  | Kommunikationsmodelle, Feedbacktools |
| 5         | 4             | - Implementierung von Konfliktlösungsstrategien                                       | B6.3  | Präsentationssoftware                |
| 5         | 4             | - Sicherstellung transparenter Entscheidungsprozesse                                  | B6.3  | MS Teams, Confluence                 |

## Lernziele
 * Vertiefung und Anwendung der Kenntnisse aus PRJ1, PRJ2, PRJ3, PRJ4
 * Weiterentwicklung und Abschlussfähigkeiten im ICT-Management
   * Analyse und Optimierung organisatorischer Prozesse
   * Integration und Bewertung neuer Technologien zur Steigerung der Effizienz
 * Abschluss eines ICT-Projekts unter Einhaltung von Budget- und Zeitvorgaben
   * Planung und Umsetzung eines Projekts mit realistischen Fallback-Szenarien
   * Professionelle Dokumentation und Übergabe an Stakeholder
 * Kommunikation und Führungsstärke in komplexen Situationen
   * Konfliktmanagement und Moderation zwischen verschiedenen Anspruchsgruppen
   * Sicherstellung transparenter Entscheidungsprozesse
 * Erweiterung der Fähigkeiten im strategischen Risikomanagement
   * Durchführung einer Risiko- und Chancenbewertung für langfristige Projekte
   * Entwicklung nachhaltiger Lösungen für wiederkehrende Herausforderungen

## Transfer
 * Erarbeitung und Umsetzung eines komplexen Projekts als Semesterarbeit
 * Praktische Übungen zur Prozessoptimierung und Implementierung neuer Technologien
 * Simulation und Analyse von Szenarien zur Entscheidungsfindung in dynamischen Umgebungen
 * Präsentation und Verteidigung eines Projekts vor einem Gremium von Experten
 * Vertiefung von Strategien für Krisenmanagement und Langzeitplanung

## Voraussetzungen

[PRJ4](./PRJ4.md)

* Fundierte Kenntnisse im ICT-Management aus PRJ4
* Nachweis praktischer Erfahrung in der Leitung von ICT-Projekten
* Bereitschaft zur Übernahme von Leitungsaufgaben in einem komplexen Umfeld
* Verständnis für interdisziplinäre Zusammenarbeit und Kommunikation

## Dispensation

siehe [PRJ1](./PRJ1.md)

* Nachweis über die erfolgreiche Leitung eines grossen ICT-Projekts mit Budget- und Personalverantwortung
* Gültige Zertifikate im Bereich Enterprise-Management oder vergleichbarer Qualifikationen
* Teilnahme an einer Weiterbildung in den Bereichen Change- und Krisenmanagement

## Technik

Repository, Markdown, GitLab, GitHub, Werkzeuge zur Prozess- und Risikoanalyse (z. B. Power BI, RiskWatch), fortgeschrittene Tools für Teamkollaboration (z. B. Jira, Confluence), Software für Präsentationen und Simulationen

## Methoden

Blended Learning, Peer Teaching, Projektbasiertes Lernen, praxisnahe Fallstudien, Simulation komplexer Projektszenarien, Workshops, Coaching durch Experten, Feedbackrunden

## Schlüsselbegriffe

Projektabschluss, Ressourcenoptimierung, Prozessintegration, Change-Management, strategisches Risikomanagement, ICT-Innovation, Budgettreue, Zeitmanagement, Stakeholder-Engagement, Konfliktlösung, Entscheidungsprozesse, Präsentationstechniken, Krisenmanagement

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Einzel- und Gruppenarbeiten, Fallstudien, Präsentationen, Semesterarbeit als Abschlussprojekt, Peer-Feedback, Praxisübungen, Expertencoaching

## Lehrmittel

* [Das Agile Manifest](https://agilemanifesto.org/)
* [HERMES (Kanton ZH)](https://hermes.zh.ch)
* Fachartikel und Studien zu ICT-Innovationen
* Dokumentationen und Tutorials zu fortgeschrittenen Tools für Prozess- und Risikoanalyse
* Literatur zu Change- und Krisenmanagement
