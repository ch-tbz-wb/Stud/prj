# Umsetzung

Projektmanagement ist ein fortlaufendes und stets gefordertes Element, unabhängig davon, ob es darum geht, Lerninhalte im Studium zu bearbeiten, Vorhaben in privaten Vereinen zu realisieren oder Projekte sowie Services in der Arbeitswelt zu entwickeln. Es ist ein integraler Bestandteil in jedem Bereich. Aus diesem Grund ist Projektmanagement ein wesentlicher Teil jedes Semesters. Neben der Vermittlung von theoretischen Konzepten und bewährten Praktiken umfasst dies auch das kontinuierliche Coaching der Studierenden bei ihren Semesterprojekten. Jede Semesterarbeit stellt ein eigenes kleines Projekt dar, in dem die Methoden des Projektmanagements angewandt und vertieft werden können.

 - [PRJ1 - Projektmanagement 1](./PRJ1.md)
 - [PRJ2 - Projektmanagement 2](./PRJ2.md)
 - [PRJ3 - Projektmanagement 3](./PRJ3.md)
 - [PRJ4 - Projektmanagement 4](./PRJ4.md)
 - [PRJ5 - Projektmanagement 5](./PRJ5.md)

