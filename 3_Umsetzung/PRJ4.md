# PRJ4 - Projektmanagement 4
 - Bereich: Eine ICT-Organisationseinheit leiten
 - Semester: 4

## Lektionen
 * Präsenz: 10
 * Virtuell: 30
 * Selbststudium: 20

 ## Fahrplan für PRJ4
| Lektionen | Selbststudium | Inhalt                                                                                 | RP   | Tools                                |
|-----------|---------------|----------------------------------------------------------------------------------------|------|--------------------------------------|
| 10        | 6             | **Strategisches Management und Organisationseffizienz**                               | B6.3 | Tableau, Power BI, Analyse-Tools     |
| 5         | 4             | - Entwicklung langfristiger Strategien zur Effizienzsteigerung                        | B6.3 | Miro, Präsentationssoftware          |
| 5         | 4             | - Optimierung der Unternehmensprozesse durch datengetriebene Entscheidungen           | B6.3 | Power BI, Excel                      |
| 10        | 6             | **Management-Kommunikation und Stakeholder-Analyse**                                  | B6.3 | Kommunikationsmodelle, Feedbacktools |
| 5         | 3             | - Erstellung professioneller Berichte und Präsentationen                              | A2.4 | Präsentationssoftware                |
| 5         | 3             | - Analyse und Deckung des Informationsbedarfs für Entscheidungsträger                 | B6.3 | Analyse-Tools                        |
| 10        | 6             | **Präventives Risikomanagement**                                                      | B6.3 | Szenario-Tools, Risikoanalyse-Tools  |
| 5         | 3             | - Etablierung von präventiven Massnahmen zur Risikobewertung                          | B14.4| Power BI, RiskWatch                  |
| 5         | 3             | - Krisenmanagement-Strategien simulieren und anwenden                                 | B14.4| Risikoanalyse-Software               |


## Lernziele
 * Vertiefung und Anwendung der Kenntnisse aus PRJ1, PRJ2, PRJ3
 * Fortgeschrittene Leitung einer ICT-Organisationseinheit
   * Vertiefte Planung und Verwaltung von Ressourcen und Budgets
   * Entwicklung langfristiger Strategien zur Organisationseffizienz
 * Kommunikations- und Präsentationsfähigkeiten auf Managementebene
   * Erstellung und Vermittlung professioneller Berichte für Entscheidungsträger
   * Nutzung geeigneter Medien und Technologien zur Zielgruppenansprache
 * Entscheidungsfindung und Stakeholder-Management
   * Aufbau effektiver Kommunikationswege mit internen und externen Stakeholdern
   * Analyse und Deckung des Informationsbedarfs für komplexe Entscheidungsprozesse
 * Erweiterung der Kompetenzen im Risikomanagement
   * Etablierung eines kontinuierlichen Risikobewertungsprozesses
   * Ableitung präventiver Massnahmen und Krisenmanagement
 * Optimierung der Unternehmensprozesse
   * Bewertung und Verbesserung der Effizienz von Geschäftsprozessen
   * Umsetzung von Verbesserungsvorschlägen auf Basis datengetriebener Entscheidungen

## Transfer
 * Entwicklung von strategischen Lösungen für reale Organisationsprobleme
 * Einsatz von Szenario-Analysen und Tools zur Entscheidungsfindung
 * Simulationen zur Kommunikation in komplexen Stakeholder-Umfeldern
 * Erstellung und Präsentation eines umfassenden Organisationsplans
 * Nutzung praxisnaher Fallstudien zur Analyse von Kommunikationsstrategien
 * Anwendung von Konzepten zur Prozessoptimierung

## Voraussetzungen

[PRJ3](./PRJ3.md)

* Fortgeschrittene Kenntnisse im Projektmanagement
* Erfahrungen in der Anwendung von Projektplanungstools und Technologien
* Nachweis von Erfahrungen im Umgang mit Stakeholder-Kommunikation
* Basiskenntnisse in Risikoanalyse und -bewertung

## Dispensation

siehe [PRJ1](./PRJ1.md)

* Nachweis über umfangreiche Erfahrung in der Leitung von Organisationseinheiten
* Gültige Zertifikate in Projekt- oder Organisationsmanagement (z. B. PMP, ITIL)
* Nachweis über Weiterbildung im Bereich strategisches Management oder Krisenmanagement

## Technik

Repository, Markdown, GitLab, GitHub, Tools zur Szenario-Analyse (z. B. Tableau, Power BI), fortgeschrittene Kommunikationsplattformen (z. B. Miro, Zoom), Software zur Entscheidungsunterstützung, datengetriebene Entscheidungsfindungs-Tools

## Methoden

Blended Learning, Peer Teaching, Projektbasiertes Lernen, Individuelles Coaching, praxisnahe Fallstudien, Simulationen komplexer Organisationsszenarien, Workshops, Feedbackschleifen, datengetriebene Prozessanalysen

## Schlüsselbegriffe

Strategisches Management, Kommunikationsstrategien, Risikobewertung, Krisenmanagement, Projektmanagement, Informationsbedarf, Stakeholder-Management, Ressourcenplanung, Zeitmanagement, Berichterstellung, Szenario-Analyse, Organisationsoptimierung, Entscheidungsfindung, Medienkompetenz, Zielgruppenorientierung, Prozessoptimierung, datengetriebene Entscheidungen

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Einzel- und Gruppenarbeiten, Fallstudien, Präsentationen, Workshops, Semesterarbeit als Gruppenprojekt, Peer-Feedback, Praxisübungen

## Lehrmittel

* [Das Agile Manifest](https://agilemanifesto.org/)
* [HERMES (Kanton ZH)](https://hermes.zh.ch)
* Fachartikel und Fallstudien zu strategischem Management
* Dokumentationen und Tutorials zu Szenario-Tools und Präsentationstechniken
* Literatur zu datengetriebener Prozessoptimierung