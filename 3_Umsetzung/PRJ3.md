# PRJ3 - Projektmanagement 3
 - Bereich: Eine ICT-Organisationseinheit leiten
 - Semester: 3

## Lektionen
 * Präsenz: 0
 * Virtuell: 30
 * Selbststudium: 20

## Fahrplan für PRJ3
| Lektionen | Selbststudium | Inhalt                                                                                 | RP   | Tools                                |
|-----------|---------------|----------------------------------------------------------------------------------------|------|--------------------------------------|
| 10         | 5             | **Leitung einer ICT-Organisationseinheit**                                             | B6.2 | Szenario-Planungstools, GitLab       |
| 5         | 3             | - Ressourcenplanung und Budgetierung                                                  | B6.2 | MS Teams, Planungstools              |
| 5         | 2             | - Mitarbeitereinsatz organisieren                                                     | B6.2 | Kollaborationsplattformen (Slack)    |
| 10         | 10             | **Stakeholder-Kommunikation sicherstellen**                                           | B6.2 | Kommunikationsmodelle, Feedbacktools|
| 5         | 5             | - Informationsbedarf der Stakeholder bestimmen                                        | B6.3 | Analyse-Tools, MS Teams              |
| 5         | 5             | - Kommunikation in Entscheidungssituationen optimieren                                | B6.3 | Präsentationssoftware                |
| 10         | 5             | **Strategische Entscheidungsfindung**                                                 | B6.3 | Szenario-Tools, Entscheidungssoftware|
| 10         | 5             | - Einsatz moderner Technologien zur Entscheidungsunterstützung                        | B6.3 | Monte-Carlo-Simulation, Analyse-Tools|

## Lernziele
 * Vertiefung und Anwendung der Kenntnisse aus PRJ1 und PRJ2
 * Geschäftsprozesse verantwortungsvoll ausführen und optimieren
   * Ressourcen und Budgets effektiv planen und verwalten
   * Mitarbeitereinsatz organisieren und Kommunikation mit Stakeholdern sicherstellen
 * Entscheidungsfindung und strategisches Management
   * Informationsbedarf für Entscheidungssituationen analysieren und decken
   * Einsatz moderner Technologien zur Entscheidungsunterstützung
 * Kommunikations- und Teamführungsfähigkeiten erweitern
   * Effiziente Kommunikation in komplexen Organisationen
   * Moderation von Konflikten und Aufbau einer konstruktiven Teamkultur
 * Fachliche Kenntnisse mit betriebswirtschaftlichem Wissen verknüpfen
   * Methoden zur Optimierung von Unternehmensprozessen anwenden
   * Erstellung und Präsentation von Vorschlägen für Prozessverbesserungen
 * Vertiefte Kenntnisse im Risikomanagement
   * Systematische Risikoanalyse und Ableitung von Massnahmen zur Risikominimierung
   * Anwendung von Szenario-Planung zur Bewältigung unvorhergesehener Ereignisse

## Transfer
 * Entwicklung von praxisorientierten Lösungen für komplexe organisatorische Herausforderungen
 * Analyse von Fallstudien zur Vertiefung strategischer Entscheidungsprozesse
 * Simulationen zur Ressourcenzuteilung und Stakeholder-Kommunikation
 * Erstellung und Präsentation von Projektplänen und Fortschrittsberichten
 * Praktische Übungen zur Budgetplanung und Risikoanalyse
 * Integration betriebswirtschaftlicher Ansätze zur Prozessoptimierung

## Voraussetzungen

[PRJ2](./PRJ2.md)

* Grundkenntnisse im Projektmanagement auf Niveau PRJ2
* Erfahrung mit grundlegenden Planungs- und Kommunikationstools wie GitLab oder MS Teams
* Verständnis von Projektphasen und Techniken des agilen Managements
* Bereitschaft zur Übernahme von Verantwortung in komplexeren organisatorischen Kontexten

## Dispensation

siehe [PRJ1](./PRJ1.md)

* Zusätzlich: Nachweis über mindestens zwei Jahre Berufserfahrung in der Leitung von ICT-Projekten
* Alternativ: Zertifikate in ITIL oder vergleichbare Qualifikationen im Bereich ICT-Management
* Gültige Weiterbildung im Bereich Risikomanagement oder Teamführung, z. B. CAS oder MAS

## Technik

Repository, Markdown, GitLab, GitHub, Einsatz moderner Planungstools (z. B. Trello, Asana, MS Project), Kommunikationsplattformen (z. B. MS Teams, Slack), Software für Szenario-Planung und Risikoanalyse (z. B. Monte-Carlo-Simulation)

## Methoden

Blended Learning, Peer Teaching, Projektbasiertes Lernen, Individuelles Coaching, praxisnahe Fallstudien, Simulationen zur Entscheidungsfindung, kollaborative Projektarbeit, Feedbackschleifen

## Schlüsselbegriffe

Projektmanagement, Managementtechniken, Pflichtenheft, Lastenheft, Evaluation, Projektarbeit, Projektmethoden, Vorgehensmodelle, Projekt-Techniken, Change Management, Projektphasen, Planungszyklus, Systems Engineering, der Mensch im Projekt, Projektführung, Zeitplanung, Ressourcenplanung, Qualitätsmanagement, Risikomanagement, ITSM, Projektkommunikation, Stakeholder, Rollen, Informationsbedarf, Entscheidungsunterstützung, Kommunikationsstrategien, Berichterstellung, Szenario-Planung, Budgetoptimierung, Teamdynamik, Prozessoptimierung, Geschäftsprozesse

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Lehrbuchzusammenfassungen, Semesterarbeit als Gruppenarbeit, Fallstudien, Praxisübungen, Workshops, Peer-Feedback

## Lehrmittel

* [Das Agile Manifest](https://agilemanifesto.org/)
* [HERMES (Kanton ZH)](https://hermes.zh.ch)
* Fachartikel und Fallstudien aus der Praxis
* Software-Dokumentationen und Tutorials zu Planungstools
* Praxisleitfäden zur Geschäftsprozessoptimierung
