# PRJ1 - Projektmanagement 1
 - Bereich: Unternehmens- und Führungsprozesse gestalten und verantworten
 - Semester: 1

## Lektionen
 * Präsenzunterricht: 30
 * Fernunterricht: 30
 * Selbststudium: 40

 ## Fahrplan für PRJ1
- **Fokus**: Einführung in Projektmanagement und Kommunikation
- **Ziele**:
  - Grundlegende Kommunikationstechniken anwenden.
  - Verständnis für Projektphasen und -strukturen entwickeln.
  - Erste Arbeitsergebnisse zielgruppengerecht präsentieren.
- **Technik**: Einführung in GitHub, Markdown.
- **Praxis**: Rollenspiele und Simulationen, erste Semesterarbeit.
- **Rahmenlehrplan-Verbindung**: A2.1 - A2.4.

## Lernziele
 * Kommunikation situationsangepasst und wirkungsvoll gestalten
   * Informationen adressatengerecht selektieren
   * Art der Kommunikation effektiv festlegen
   * Arbeitsergebnisse zielgruppenadäquat mit geeigneten medialen und rhetorischen Elementen zu präsentieren
 * Teamführung und Sozialkompetenz entwickeln
   * Verknüpfung zwischen Projektmanagement und Sozialkompetenz
   * Einfluss von Kommunikation auf Projekterfolg
 * Technische Fähigkeiten im Projektmanagement erweitern
   * Kennenlernen von modernen Projektmanagement-Tools und -Software
   * Einsatz von Technologie in Projektplanung und -durchführung

## Transfer
 - Effektiver Kommunikation in Projektmanagement in praxisnahen Projekten erfahren 
 - Rollenspiele und Simulationen, in denen die Studierenden unterschiedliche Rollen übernehmen
 - Projektmanagementfähigkeiten mit kompetenzorientierte Bewertungsmethoden 
 - Übungen zu Selbsteinschätzung, Feedback und Reflexion über geleistete Arbeit
 - Individuelle Coaching durch Dozenten
 - Moderne Projektmanagement-Tools und -Software ausprobieren und in eigener Semesterarbeit einsetzen
 - Gastvorträge von erfahrenen Projektmanagern aus der IT-Branche zu  Relevanz und Anwendung von Moderne Projektmanagement-Tools und -Software

## Voraussetzungen
Optional: Module aus der Grundbildung (Inhalte siehe [Modulbaukasten.ch](https://www.modulbaukasten.ch/)): 262 Evaluation von ICT-Mitteln durchführen, 431 Aufträge im eigenen Berufsumfeld selbstständig durchführen

## Dispensation
Zertifizierung aus dem IPMA-Programm mit Level A und B (International Project Management Association), Zertifizierung Prince2,  Scrum-Master mit mind. 3 Jahren praktischer Erfahrung, CAS, MAS oder DAS in Projektmanagement, mind. 3 Jahre praktische, Projektmanagererfahrungen mit komplexen Informatikprojekten, Gleichwertige oder Vergleichbare Ausbildung im Umfang von 200h
 
 _**Hinweis:** Die Lektionen im Zusammenhang mit den Semesterarbeiten können nicht dispensiert werden, daher diese Promotionsrelevant sind._

## Technik
GitHub, GitLab, Markdown, Projektmanagementspezifische Tools, Repository

## Methoden
Blended Learning, Einzelarbeit, Flipped Classroom, Frontalunterricht, Gruppenarbeit, Microlearning, Peer Teaching, Projektbasiertes Lernen

## Schlüsselbegriffe
agiles Projektmanagements, Managementtechniken, Planungszyklus, Projektphasen, Implementierung und Überwachung, Change Management, Evaluation, ITSM, Projektkommunikation, Rollen, Stakeholder-Management, Konfliktmanagement, Projektführung, Fallback-Szenarien, Lastenheft, Pflichtenheft, Projekt-Techniken, Projektmethoden, Ressourcenplanung, Vorgehensmodelle, Zeitplanung (Gantt-Diagramme), Point of No Return (PONR), Qualitätsmanagement, Risikomanagement

## Lehr- und Lernformen
 Einzel- und Gruppenarbeiten, Lehrbuchzusammenfassungen, Lehrgespräche, Lehrervorträge, Lernfragen, Präsentationen, Semesterarbeit als Gruppenarbeit

## Lehrmittel
 - [Das Agile Manifest](https://agilemanifesto.org/)
 - [HERMES (Kanton ZH)](https://hermes.zh.ch)
 - [Organisationswürfel](http://www.logisch.ch/OLK/Organisationsmanagement/einfuehrung.htm)
 - [SEUSAG](https://www.ibo.de/glossar/definition/Systemdenken-und-SEUSAG)
 - TBZ-eigenes Repository "Projektmanagment"
